/*eslint-disable*/
import { expect } from 'chai';
import spotify from '../src/Spotify';

describe('Spotify', () => {
  it('should be an spotify object', () => {
    expect(spotify).to.be.an('object');
  });

  it('should have the search method', () => {
    expect(spotify.search).to.exist;
  });

  it('should have the album method', () => {
    expect(spotify.album).to.exist;
  });
});
